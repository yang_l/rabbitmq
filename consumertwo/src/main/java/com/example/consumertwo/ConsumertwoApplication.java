package com.example.consumertwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yang_li
 */
@SpringBootApplication
public class ConsumertwoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumertwoApplication.class, args);
    }

}
