package com.example.consumertwo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2818:10
 */
@Component
@Slf4j
public class FanoutListener {

    /**
     * 这里是直接在控制台创建的队列
     *
     * @param message  ·
     */
    @RabbitListener(queues = "fanout.queue2")
    public void fanoutQueue1Listener(String message){
      log.info("收到交换机fanout.yangl广播的消息：{}", message);
    }

    /**
     * 测试 监听 通过config创建的队列
     *
     * @param message `
     */
    @RabbitListener(queues = "fanoutBean.queue2")
    public void fanoutBeanQueue2Listener(String message){
        log.info("收到交换机yangl.fanoutBean广播的消息：{}", message);
    }

}
