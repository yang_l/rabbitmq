package com.example.consumertwo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2911:17
 */

@Component
@Slf4j
public class TopicListener {

    /**
     * 这里是直接在控制台创建的队列
     *
     * @param message  ·
     */
    @RabbitListener(queues = "topic.queue2")
    public void topicQueue2(String message){
        log.info("收到 topic.queue2的消息：{}" ,message);
    }

    /**
     * 测试通过Bean创建的队列能否被监听到
     *
     * @param message ·
     */
    @RabbitListener(queues = "topicBean.queue2")
    public void topicBeanQueue2(String message){
        log.info("收到 topicBean.queue2的消息：{}" ,message);
    }
}
