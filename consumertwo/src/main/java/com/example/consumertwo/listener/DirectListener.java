package com.example.consumertwo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2910:07
 */

@Component
@Slf4j
public class DirectListener {

    /**
     * 这里是直接在控制台创建的队列
     *
     * @param message  ·
     */
    @RabbitListener(queues = "direct.queue2")
    public void directQueue1Listener(String message){
      log.info("收到direct.queue2的消息：{}", message);
    }

    /**
     * 测试 能否监听到通过Bean创建的队列
     *
     * @param message ·
     */
    @RabbitListener(queues = "directBean.queue2")
    public void directBeanQueue1Listener(String message){
        log.info("收到directBean.queue1的消息：{}", message);
    }
}
