package com.example.consumertwo.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2810:17
 */

@Component
@Slf4j
public class RabbitMqListener {

    @RabbitListener(queues = "work.queue")
    public void listenWorkQueue(String message) {
        try {
            Thread.sleep(1000);
            log.info("消费者2收到的消息：{}", message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
