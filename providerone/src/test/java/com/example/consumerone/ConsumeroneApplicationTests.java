package com.example.consumerone;

import com.example.providerone.ProvideroneApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// 这里注意需要使用SpringJUnit4ClassRunner.class的运行环境
@RunWith(SpringJUnit4ClassRunner.class)
// 这里记得配置启动类，不然找不到自动注入的类
@SpringBootTest(classes = ProvideroneApplication.class)
public class ConsumeroneApplicationTests {

    // 这里的test注意导包
    @Test
    public void contextLoads() {
        System.out.println("ss");
    }


}
