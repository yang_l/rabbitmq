package com.example.consumerone.rabbitMqTest;

import com.example.providerone.ProvideroneApplication;
import com.example.providerone.service.RabbitMqService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * @Author: yang_li
 * @Date: 2024/3/2714:38
 */
// 这里注意需要使用SpringJUnit4ClassRunner.class的运行环境
@RunWith(SpringJUnit4ClassRunner.class)
// 这里记得配置启动类，不然找不到自动注入的类
@SpringBootTest(classes = ProvideroneApplication.class)
public class RabbitMqTest {

    @Resource
    private RabbitMqService rabbitMqService;

    // 这里的test注意导包
    @Test
    public void sendRabbitMqMessageToQueueTest(){
        Assert.assertEquals("success",rabbitMqService.sendRabbitMqMessageToQueue().getMessage());
    }

    @Test
    public void sendRabbitMqMessageToWorkQueueTest(){
        Assert.assertEquals("success",rabbitMqService.sendRabbitMqMessageToWorkQueue().getMessage());
    }

    @Test
    public void fanoutExchange(){
        Assert.assertEquals("success",rabbitMqService.fanoutExchange().getMessage());
    }

    @Test
    public void directExchange(){
        Assert.assertEquals("success",rabbitMqService.directExchange().getMessage());
    }

    @Test
    public void topicExchange(){
        Assert.assertEquals("success",rabbitMqService.topicExchange().getMessage());
    }

    @Test
    public void fanoutExchangeBean(){
        Assert.assertEquals("success",rabbitMqService.fanoutExchangeBean().getMessage());
    }

    @Test
    public void directExchangeBean(){
        Assert.assertEquals("success",rabbitMqService.directExchangeBean().getMessage());
    }

    @Test
    public void topicExchangeBean(){
        Assert.assertEquals("success",rabbitMqService.topicExchangeBean().getMessage());
    }

    @Test
    public void fanoutAnnotationExchange(){
        Assert.assertEquals("success",rabbitMqService.fanoutAnnotationExchange().getMessage());
    }

    @Test
    public void directAnnotationExchange(){
        Assert.assertEquals("success",rabbitMqService.directAnnotationExchange().getMessage());
    }

    @Test
    public void topicAnnotationExchange(){
        Assert.assertEquals("success",rabbitMqService.topicAnnotationExchange().getMessage());
    }

    @Test
    public void serializeTest(){
        Assert.assertEquals("success",rabbitMqService.serializeTest().getMessage());
    }

}



































