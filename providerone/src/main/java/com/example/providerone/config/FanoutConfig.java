package com.example.providerone.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: yang_li
 * @Date: 2024/3/2914:05
 */

@Configuration
public class FanoutConfig {

    /**
     * 声明fanout交换机
     */
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange("yangl.fanoutBean");
    }

    /**
     * 声明 第一个队列
     */
    @Bean
    public Queue fanoutBeanQueue1(){
        return new Queue("fanoutBean.queue1");
    }

    /**
     * 声明第二队列
     */
    @Bean
    public Queue fanoutBeanQueue2(){
        return new Queue("fanoutBean.queue2");
    }

    /**
     * 绑定 第一个队列队列和交换机
     */
    @Bean
    public Binding bindingQueue1(Queue fanoutBeanQueue1 ,FanoutExchange fanoutBeanExchange){
        return BindingBuilder.bind(fanoutBeanQueue1).to(fanoutBeanExchange);
    }

    /**
     * 绑定第二个交换机
     */
    @Bean
    public Binding bindingQueue2(Queue fanoutBeanQueue2, FanoutExchange fanoutExchange){
        return BindingBuilder.bind(fanoutBeanQueue2).to(fanoutExchange);
    }

}
