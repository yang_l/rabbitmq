package com.example.providerone.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: yang_li
 * @Date: 2024/3/2918:03
 */
@Configuration
public class MessageConfig {

    /**
     * 自定义消息转换器
     *
     * @return ·
     */
    @Bean
    public MessageConverter messageConverter(){
        // 自定义消息转换器
        final Jackson2JsonMessageConverter jackson2JsonMessageConverter = new Jackson2JsonMessageConverter();
        // 配置自动创建消息id,用于识别不同的消息，也可以在业务中基于id判断是否消息重复
        jackson2JsonMessageConverter.setCreateMessageIds(true);
        return jackson2JsonMessageConverter;
    }

}
