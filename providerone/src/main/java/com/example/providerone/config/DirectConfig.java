package com.example.providerone.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: yang_li
 * @Date: 2024/3/2914:17
 */

@Configuration
public class DirectConfig {

    /**
     * 声明一个direct类型的交换机
     */
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("yangl.directBean");
    }

    /**
     * 声明 第一个队列
     */
    @Bean
    public Queue directBeanQueue1(){
        return new Queue("directBean.queue1");
    }

    /**
     * 声明第二个队列
     */
    @Bean
    public Queue directBeanQueue2(){
        return new Queue("directBean.queue2");
    }

    /**
     * 绑定第一个队列并 binding 一个 routing
     */
    @Bean
    public Binding bindingQueue1WithRed(Queue directBeanQueue1 , DirectExchange directExchange){
        return BindingBuilder.bind(directBeanQueue1).to(directExchange).with("red");
    }

    /**
     * binding 第一个队列，并binding另外一个 routing key
     */
    @Bean
    public Binding bindingQueue1WithBlue(Queue directBeanQueue1 ,DirectExchange directExchange){
        return BindingBuilder.bind(directBeanQueue1).to(directExchange).with("blue");
    }

    /**
     * 绑定第二个队列 并binding一个 routing key
     */
    @Bean
    public Binding bindingQueue2WithRed(Queue directBeanQueue2 ,DirectExchange directExchange){
        return BindingBuilder.bind(directBeanQueue2).to(directExchange).with("red");
    }

    /**
     * 绑定第二个队列 并binding另外个 routing key
     */
    @Bean
    public Binding bindingQueue2WithYellow(Queue directBeanQueue2 ,DirectExchange directExchange){
        return BindingBuilder.bind(directBeanQueue2).to(directExchange).with("yellow");
    }

}
