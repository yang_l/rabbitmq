package com.example.providerone.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: yang_li
 * @Date: 2024/3/2914:36
 */
@Configuration
public class Topicconfig {

    /**
     * 声明一个topic类型的交换机
     */
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("yangl.topicBean");
    }

    /**
     * 声明 第一个队列
     */
    @Bean
    public Queue topicBeanQueue1(){
        return new Queue("topicBean.queue1");
    }

    /**
     * 声明第二个队列
     */
    @Bean
    public Queue topicBeanQueue2(){
        return new Queue("topicBean.queue2");
    }

    /**
     * 绑定第一个队列并 binding 一个 routing
     */
    @Bean
    public Binding bindingQueue1WithChina(Queue topicBeanQueue1 , TopicExchange topicExchange){
        return BindingBuilder.bind(topicBeanQueue1).to(topicExchange).with("china.#");
    }


    /**
     * 绑定第二个队列 并binding另外个 routing key
     */
    @Bean
    public Binding bindingQueue2WithNews(Queue topicBeanQueue2 ,TopicExchange topicExchange){
        return BindingBuilder.bind(topicBeanQueue2).to(topicExchange).with("#.news");
    }

}
