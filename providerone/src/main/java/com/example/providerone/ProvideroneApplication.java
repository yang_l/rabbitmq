package com.example.providerone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * @author yang_li
 */
@SpringBootApplication
public class ProvideroneApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvideroneApplication.class, args);
    }

}
