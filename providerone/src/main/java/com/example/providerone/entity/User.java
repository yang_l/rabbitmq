package com.example.providerone.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Author: yang_li
 * @Date: 2024/3/2917:45
 */

@Data
@AllArgsConstructor
public class User {

    private String name;

    private Integer age;
}
