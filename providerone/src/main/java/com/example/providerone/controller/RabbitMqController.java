package com.example.providerone.controller;

import com.example.providerone.result.Result;
import com.example.providerone.service.RabbitMqService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: yang_li
 * @Date: 2024/3/279:52
 */
@RestController
@RequestMapping("rabbitmq")
public class RabbitMqController {

    // 本文文字地址：https://blog.csdn.net/weixin_73077810/article/details/133836287

    @Resource
    private RabbitMqService rabbitMqService;

    /**
     * 第一种发送消息的方式   将消息直接发送到指定队列中  不经过exchange（交换机） 发送一个消息
     *
     * @return 结果
     */
    @GetMapping("sendRabbitMqMessageToSimpleQueue")
    public Result<String> testSimpleQueue(){
        return rabbitMqService.sendRabbitMqMessageToQueue();
    }

    /**
     * 这里想测试的是  向一个队列中发送多个消息，让多个消费者来消费
     *
     * @return 结果
     */
    @GetMapping("sendRabbitMqMessageToWorkQueue")
    public Result<String> testWorkQueue(){
        return rabbitMqService.sendRabbitMqMessageToWorkQueue();
    }

    /*
     * 上面两个测试案例中都是直接向队列中发送消息，没有经过交换机，
     * 而一旦引入交换机，消息发送的模式会有很大的变化
     * exchange(交换机) 只转发消息，不具备储备消息的能力，因此如果没有任何队列与exchange绑定，
     * 或者没有符合路由规则的队列，那么消息会丢失
     * exchange有4种类型：
     * fanout：广播 将消息交给所有绑定到交换机的队列，
     * direct：订阅 基于routing key （路由key） 发送给订阅了消息的队列
     * topic：通配符订阅，与direct类似，只不过routing key可以使用通配符
     * headers：头匹配，基于mq的消息头匹配，几乎不怎么使用
     *
     * 交换机的作用？
     * 1 接收publish的消息
     * 2 将消息按照路由规则路由到与之绑定的队列
     * 3 不能缓存消息，路由失败，消息丢失
     * 4 fanoutExchange的会将消息路由到每个绑定的队列中
     */

    /**
     * 这里测试一下fanout模式的消息发送
     *
     * @return result
     */
    @GetMapping("fanoutExchange")
    public Result<String> fanoutExchange(){
        return rabbitMqService.fanoutExchange();
    }

    /**
     * direct 交换机
     * 队列与交换机的绑定，不能是任意绑定了，而是要指定一个routing key
     * 消息的发送方在想exchange发送消息时，也必须指定routing key
     * exchange不再把消息交给每一个绑定的队列，而是根据消息的routing key进行判断，
     * 只有队列的routing key 与 交换及的routing key完全一致才会接收到消息
     *
     * 总结direct交换机和fanout交换机的区别？
     * fanout会将消息发送给每一个binding的queue
     * direct 会根据 routing key 判读 具体路由给哪一个队列
     * 如果多个队列具有相同的routing key 则与fanout功能类似
     *
     * @return result
     */
    @GetMapping("directExchange")
    public Result<String> directExchange(){
        return rabbitMqService.directExchange();
    }

    /**
     * topic 交换机
     * 虽然我们使用direct交换机改进了我们的系统，但是它仍然存在局限性，比方说我们的交换机binding了对各不同的routing key，在
     * direct模式中，虽然能做到有选择性的接收消息，但是他们的选择性是单一的，每一个routing key都需要去指定，只能被一个相同的binding
     * 消费，我们想让它的选择变得多元，比如划分一个子组，一个消息可以根据一个组别的队列进行投递，那么就需要用到topic模式
     *
     * 举例，比如现在有 routing key ，a  a.b  a.b.c  a.b.d.c... 等
     * 如果我想把一个消息投递到 routing key 为 a 开头的队列中，就需要在交换机中将所有a开头的队列binding到交换机，
     * 如果a开头的routing key很多，那么我们就需要去binding很对的队列
     * 并且在direct中发消息的时候，每一个消息都需要指定routing key  这个时候，就需要发很多次消息，变得很复杂
     *
     * 而 topic 类型的exchange可以让队列在binding的时候使用通配符
     * binding key一般由一个或多个单词组成，多个单词之间以 . 分割
     * 通配符规则：
     * # ：匹配 0个或多个
     * *：匹配不多不少一个
     *
     * 与direct的区别就是  topic可以使用通配符
     */
    @GetMapping("topicExchange")
    public Result<String> topicExchange(){
        return rabbitMqService.topicExchange();
    }

    /*
     * 上面所有的测试  我们都是在rabbitmq 控制台创建的exchange 和 queue
     * 但是在实际开发过程中如果采用这种方式，我们交给运维的时候，还需要将交换机和队列给写出来，
     * 在这个过程中是很容易出现错误的，我们应该在代码中去创建队列和交换机，
     * 推荐的做法是有程序启动的时候检查队列和交换机是否存在如果不存在，则自动创建
     * 我们通过bean的方式来创建交换机和队列  详情请看 config文件夹下的内容
     */

    /**
     * 接下来我们尝试像我们通过Bean创建的交换机和队列中发送消息
     */
    @GetMapping("fanoutExchangeBean")
    public Result<String> fanoutExchangeBean(){
        return rabbitMqService.fanoutExchangeBean();
    }

    @GetMapping("directExchangeBean")
    public Result<String> directExchangeBean(){
        return rabbitMqService.directExchangeBean();
    }

    @GetMapping("topicExchangeBean")
    public Result<String> topicExchangeBean(){
        return rabbitMqService.topicExchangeBean();
    }

    /*
     * 上面我们通过注入Bean的方式创建了交换机和队列
     * 但是我们发现在使用Bean创建交换机和队列时，使用direct和topic进行绑定的时候  会非常麻烦
     * 每一个routing key都需要写一次代码  太麻烦了
     * 那么我们接下来使用基于注解的方式来声明
     * 这种方式 应该放在消费端来进行创建 请看consumer中的代码
     * link -> com.example.consumertwo.listener.AnnotationExchange
     */

    /**
     * 向通过注解创建的fanout交换机和队列发送消息
     *
     * @return ·
     */
    @GetMapping("fanoutAnnotationExchange")
    public Result<String> fanoutAnnotationExchange(){
        return rabbitMqService.fanoutAnnotationExchange();
    }

    /**
     * 向通过注解创建的direct交换机和队列发送消息
     *
     * @return ·
     */
    @GetMapping("directAnnotationExchange")
    public Result<String> directAnnotationExchange(){
        return rabbitMqService.directAnnotationExchange();
    }

    /**
     * 向通过注解创建的topic交换机和队列发送消息
     *
     * @return ·
     */
    @GetMapping("topicAnnotationExchange")
    public Result<String> topicAnnotationExchange(){
        return rabbitMqService.topicAnnotationExchange();
    }

    /*
    总结：
    上面就是关于rabbitmq关于 交换机，队列 创建，发送，以及监听的内容了
    需要联系消费者的代码一起看 这样比较容易理解一些
    消息提供者，我虽然提供了接口，但是通过接口来访问比较麻烦
    所以我也写了单元测试，可以直接在test中运行
     */

    @GetMapping("serializeTest")
    public Result<String> serializeTest(){
        return rabbitMqService.serializeTest();
    }

    /*
    总结：
    同步调用的缺点：
    1 拓展性差
    2 性能下降
    3 级联失败
    消息队列是对同步调用的一种优化方案，将不是很重要的链路，解放出来，异步去执行
    异步调用的优点：
    1 耦合度降低
    2 性能更好
    3 业务拓展性强
    4 故障隔离，避免级联失败
    缺点：
    完全依赖于broker的可靠性，安全性和性能
    架构复杂，后期维护和调试麻烦
     */
}
