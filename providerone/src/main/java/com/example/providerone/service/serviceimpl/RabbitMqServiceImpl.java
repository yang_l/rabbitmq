package com.example.providerone.service.serviceimpl;

import com.example.providerone.entity.User;
import com.example.providerone.result.Result;
import com.example.providerone.service.RabbitMqService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author: yang_li
 * @Date: 2024/3/2714:32
 */
@Service
public class RabbitMqServiceImpl implements RabbitMqService {

    private static final String RED_MESSAGE = "红色警报！日本乱排核废水，导致海洋生物变异，惊现哥斯拉！";

    private static final String BLUE_MESSAGE = "蓝色警报！今天天气很好，大家可以出门消费了！";

    private static final String YELLOW_MESSAGE = "黄色警报！今天会出现风暴，大家尽量不要出门，注意安全！";

    private static final String TOPIC_MESSAGE = "喜报！孙悟空大战哥斯拉，胜";

    private static final String SUCCESS = "success";

    private static final String CHINA_NEWS = "china.news";

    private static final String CHINA = "china";

    private static final String TEST_MULTI = "测试 .# 是否匹配0个";

    private static final String RED = "red";

    private static final String BLUE = "blue";

    private static final String YELLOW = "yellow";

    private static final Integer FIFTY = 50;

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 第一种发送消息的方式   将消息直接发送到指定队列中  不经过exchange（交换机）
     *
     * @return 结果
     */
    @Override
    public Result<String> sendRabbitMqMessageToQueue() {
        // 队列名称
        String queueName = "simple.queue";
        // 发送的消息
        String message = "hello, word!!";
        try {
            // routing key 就是队列名称
            rabbitTemplate.convertAndSend(queueName, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok(SUCCESS);
    }

    /**
     * 这里给了两个消费者，consumerone consumertwo 它们处理消息需要消耗的时间不一致，但是结果确实消费了一样多的消息
     * 说明没有按消费者的消费能力来分发消息，属于是 如果有两个消费者就一人消费一半，采用的是负载均衡的策略，但是我们实际开发中
     * 应该按消费者消费能力来分发消息，谁消费快就多消费一点  要解决这个问题也很简单 只需要在 yml文件中添加
     * listener：simple:prefetch
     * 上面这个意思就是不采用轮询每次取一个消息，消费完了就又去取
     * 在consumer yml中添加如上配置之后，就会按消费者能力来消费了
     *
     * @return 结果
     */
    @Override
    public Result<String> sendRabbitMqMessageToWorkQueue() {
        // 队列名称
        String queueName = "work.queue";
        // 发送的消息
        String message = "hello, word!!";
        try {
            for (int i = 0; i < FIFTY; i++) {
                // routing key 就是队列名称
                rabbitTemplate.convertAndSend(queueName, message + (i + 1));
                Thread.sleep(200);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.ok(SUCCESS);
    }

    /**
     * 在测试这个之前需要先创建交换机，然后创建一些队列来绑定到交换机
     * 这个步骤我们在 rabbitmq的web端创建 交换机名字为 ：yangl.fanout
     * 这里我们不在关注队列名称了，只要绑定了交换机的队列，交换机都会发消息给它
     *
     * @return result
     */
    @Override
    public Result<String> fanoutExchange() {
        String exchangeName = "yangl.fanout";
        String message = "hello fanout !";
        rabbitTemplate.convertAndSend(exchangeName, "", message);
        return Result.ok(SUCCESS);
    }

    /**
     * 还是需要先声明交换机和队列
     * 声明一个交换机 yangl.direct
     * 声明一个队列 direct.queue1 绑定yangl.direct交换机，binding key为 blue和read
     * 声明一个队列 direct.queue2 绑定yangl.direct交换机，binding key为 yellow和read
     * 在consumer服务中分别监听direct.queue1 direct.queue2
     *
     * @return result
     */
    @Override
    public Result<String> directExchange() {
        // 交换机名称
        String exchangeName = "yangl.direct";
        // 这一条两个队列都订阅了  应该都能收到
        rabbitTemplate.convertAndSend(exchangeName, RED, RED_MESSAGE);
        // 这一条满足direct.queue1的routing key direct.queue1应该能收到
        rabbitTemplate.convertAndSend(exchangeName, BLUE, BLUE_MESSAGE);
        // 这一条满足direct.queue2的routing key direct.queue2应该能收到
        rabbitTemplate.convertAndSend(exchangeName, YELLOW, YELLOW_MESSAGE);
        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> topicExchange() {
        String exchangeName = "yangl.topic";
        rabbitTemplate.convertAndSend(exchangeName, CHINA_NEWS, TOPIC_MESSAGE);
        rabbitTemplate.convertAndSend(exchangeName, CHINA, TEST_MULTI);
        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> fanoutExchangeBean() {
        String exchangeName = "yangl.fanoutBean";
        String message = "测试通过config方式创建交换机和队列";
        rabbitTemplate.convertAndSend(exchangeName, "", message);
        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> directExchangeBean() {
        // 交换机名称
        String exchangeName = "yangl.directBean";
        // 这一条两个队列都订阅了  应该都能收到
        rabbitTemplate.convertAndSend(exchangeName, RED, RED_MESSAGE);
        // 这一条满足direct.queue1的routing key direct.queue1应该能收到
        rabbitTemplate.convertAndSend(exchangeName, BLUE, BLUE_MESSAGE);
        // 这一条满足direct.queue2的routing key direct.queue2应该能收到
        rabbitTemplate.convertAndSend(exchangeName, YELLOW, YELLOW_MESSAGE);
        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> topicExchangeBean() {
        String exchangeName = "yangl.topicBean";
        rabbitTemplate.convertAndSend(exchangeName, CHINA_NEWS, TOPIC_MESSAGE);
        rabbitTemplate.convertAndSend(exchangeName, CHINA, TEST_MULTI);
        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> fanoutAnnotationExchange() {
        String exchangeName = "yangl.fanoutAnnotation";
        String message = "测试通过注解方式创建交换机和队列";
        rabbitTemplate.convertAndSend(exchangeName, "", message);
        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> directAnnotationExchange() {
        // 交换机名称
        String exchangeName = "yangl.directAnnotation";
        // 这一条两个队列都订阅了  应该都能收到
        rabbitTemplate.convertAndSend(exchangeName, RED, RED_MESSAGE);
        // 这一条满足direct.queue1的routing key direct.queue1应该能收到
        rabbitTemplate.convertAndSend(exchangeName, BLUE, BLUE_MESSAGE);
        // 这一条满足direct.queue2的routing key direct.queue2应该能收到
        rabbitTemplate.convertAndSend(exchangeName, YELLOW, YELLOW_MESSAGE);

        return Result.ok(SUCCESS);
    }

    @Override
    public Result<String> topicAnnotationExchange() {
        String exchangeName = "yangl.topicAnnotation";
        rabbitTemplate.convertAndSend(exchangeName, CHINA_NEWS, TOPIC_MESSAGE);
        rabbitTemplate.convertAndSend(exchangeName, CHINA, TEST_MULTI);
        return Result.ok(SUCCESS);
    }

    /*
     * 通过上面的  convertAndSend() 这个方法可以看出  所有发送消息的时候 都是传入的 String 类型的消息
     * 但是mq在数据传输时，会将我们传入的消息，序列化为字节发送给MQ，接收消息的时候，还会把字节反序列化为Java对象，
     * 默认情况下Spring使用的是JDK序列化，  jdk序列化存在一下问题：
     * 1 数据体积大
     * 2 有安全漏洞
     * 3 可读性差
     * 所以我们希望消息体更小，可读性更高，因此我们可以使用JSON方式来做序列化和反序列化
     * 在 消息提供方 和 消费者方都要使用JSON来做
     * 具体实现在 config文件夹下的  messageConfig中
     */

    @Override
    public Result<String> serializeTest() {
        rabbitTemplate.convertAndSend("yangl.object", "test", new User("柳岩", 21));
        return Result.ok(SUCCESS);
    }


}
