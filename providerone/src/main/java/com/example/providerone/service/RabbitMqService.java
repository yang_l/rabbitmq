package com.example.providerone.service;

import com.example.providerone.result.Result;

/**
 * @Author: yang_li
 * @Date: 2024/3/2714:30
 */

public interface RabbitMqService {

    /**
     * 直接发送消息到队列中
     *
     * @return 结果信息
     */
    Result<String> sendRabbitMqMessageToQueue();

    /**
     * 直接发送消息到队列中,循环发送多个消息 到workqueue
     *
     * @return 结果信息
     */
    Result<String> sendRabbitMqMessageToWorkQueue();

    /**
     * 测试 fanout模式的消息发送
     *
     * @return result
     */
    Result<String> fanoutExchange();

    /**
     * 测试direct模型交换机发送消息
     *
     * @return result
     */
    Result<String> directExchange();

    /**
     * 测试topic模型交换机发送消息
     *
     * @return result
     */
    Result<String> topicExchange();

    /**
     * 测试向自己通过config方式创建的交换机中发送消息
     *
     * @return result
     */
    Result<String> fanoutExchangeBean();

    /**
     * 测试向自己通过config方式创建的交换机中发送消息
     *
     * @return result
     */
    Result<String> directExchangeBean();

    /**
     * 测试向自己通过config方式创建的交换机中发送消息
     *
     * @return result
     */
    Result<String> topicExchangeBean();

    /**
     * 测试向自己通过注解方式创建的交换机中发送消息
     *
     * @return result
     */
    Result<String> fanoutAnnotationExchange();

    /**
     * 测试向自己通过注解方式创建的交换机中发送消息
     *
     * @return result
     */
    Result<String> directAnnotationExchange();

    /**
     * 测试向自己通过注解方式创建的交换机中发送消息
     *
     * @return result
     */
    Result<String> topicAnnotationExchange();

    /**
     * 使用json序列化测试
     *
     * @return ·
     */
    Result<String> serializeTest();

}
