package com.example.consumerone.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2818:10
 */
@Component
@Slf4j
public class FanoutListener {

    /**
     * 广播  交换机将收到的消息广播到订阅了此交换机的队列中
     *
     * @param message result
     */
    @RabbitListener(queues = "fanout.queue1")
    public void fanoutQueue1Listener(String message){
      log.info("收到交换机fanout.yangl广播的消息：{}", message);
    }

    /**
     * 测试 监听 通过config创建的队列
     *
     * @param message `
     */
    @RabbitListener(queues = "fanoutBean.queue1")
    public void fanoutBeanQueue1Listener(String message){
        log.info("收到交换机fanout.yangl广播的消息：{}", message);
    }
}
