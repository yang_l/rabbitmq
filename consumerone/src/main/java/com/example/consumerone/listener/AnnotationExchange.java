package com.example.consumerone.listener;

import com.example.consumerone.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;


/**
 * @Author: yang_li
 * @Date: 2024/3/2916:11
 */

@Component
@Slf4j
public class AnnotationExchange {

    /**
     * 注解声明 监听 fanout 队列中的信息
     *
     * @param message `
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(name = "yangl.fanoutAnnotation", type = ExchangeTypes.FANOUT),
            value = @Queue(name = "fanoutAnnotation.queue1")
    ))
    public void annotationFanoutExchangeListener(String message){
        log.info("收到注解声明的交换机发到fanoutAnnotation.queue1的消息：{}" ,message);
    }

    /**
     * 注解声明 监听 direct 队列中的信息
     *
     * @param message `
     */
    @RabbitListener(bindings =
        @QueueBinding(
                exchange = @Exchange(name = "yangl.directAnnotation"),// 默认 类型是 type = ExchangeTypes.DIRECT
                value = @Queue(name = "directAnnotation.queue1"),
                key = {"red", "blue"}
        ))
    public void annotationDirectExchangeListener(String message){
        log.info("收到注解声明的交换机发到topicAnnotation.queue1的消息：{}" ,message);
    }

    /**
     * 注解声明 监听 topic 队列中的信息
     *
     * @param message `
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(name = "yangl.topicAnnotation", type = ExchangeTypes.TOPIC),
            value = @Queue(name = "topicAnnotation.queue1"),
            key = {"china.#"}
    ))
    public void annotationTopicExchangeListener(String message ){
        log.info("收到注解声明的交换机发到topicAnnotation.queue1的消息：{}" ,message);
    }

    /**
     * 测试序列化
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(name = "yangl.object"),
            value = @Queue(name = "object.queue1"),
            key = {"test"}
            ))
    public void serializeMessage(User user){
        log.info("收到的消息：{}", user.toString());
        log.info("用户名称：{}", user.getName());
        log.info("用户年龄：{}", user.getAge());
    }
}
