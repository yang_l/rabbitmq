package com.example.consumerone.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2717:33
 */

@Component
@Slf4j
public class RabbitMqListener {

    /** 利用rabbitListener来申明要监听的队列  可以看见方法体中接收的消息就是消息体的内容
     *
     * @param message 接收的消息
     * @return 结果
     */
    @RabbitListener(queues = "simple.queue")
    public void listenSimpleQueue(String message){
        log.info("消费者1收到的消息：{}", message);
    }

    @RabbitListener(queues = "work.queue")
    public void listenWorkQueue(String message){
        try {
            Thread.sleep(200);
            log.info("消费者1收到的消息：{}", message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
