package com.example.consumerone.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: yang_li
 * @Date: 2024/3/2911:17
 */

@Component
@Slf4j
public class TopicListener {

    @RabbitListener(queues = "topic.queue1")
    public void topicQueue1(String message){
        log.info("收到 topic.queue1的消息：{}" ,message);
    }

    /**
     * 测试通过Bean创建的队列能否被监听到
     *
     * @param message ·
     */
    @RabbitListener(queues = "topicBean.queue1")
    public void topicBeanQueue2(String message){
        log.info("收到 topicBean.queue1的消息：{}" ,message);
    }
}
