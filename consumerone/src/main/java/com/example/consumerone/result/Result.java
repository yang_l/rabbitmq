package com.example.consumerone.result;

import lombok.Data;

@Data
public class Result<T> {


    /**
     * 状态码
     */
    private Integer code;

    /**
     * 信息
     */
    private String message;

    /**
     * 数据
     */
    private T data;

    public Result() {
    }

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(ResultCode resultCode, T data) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMsg();
        this.data = data;
    }


    /**
     * 返回成功
     *
     * @param message 信息
     * @return 公共返回类
     */
    public static <T> Result<T> ok(String message) {
        return new Result<>(Code.OK, message);
    }

    /**
     * 返回成功
     *
     * @param <T> 数据
     * @return 公共返回类
     */
    public static <T> Result<T> ok(T data) {
        return new Result<>(ResultCode.OK, data);
    }

    /**
     * 返回成功
     *
     * @param message 信息
     * @param <T>     数据
     * @return 公共返回类
     */
    public static <T> Result<T> ok(String message, T data) {
        return new Result<>(Code.OK, message, data);
    }

    /**
     * 返回失败
     *
     * @param message 信息
     * @return 公共返回类
     */
    public static <T> Result<T> fail(String message) {
        return new Result<>();
    }

    /**
     * 返回失败
     *
     * @param <T> 数据
     * @return 公共返回类
     */
    public static <T> Result<T> fail(T data) {
        return new Result<>(ResultCode.FAIL, data);
    }

    /**
     * 返回失败
     *
     * @param message 信息
     * @param <T>     数据
     * @return 公共返回类
     */
    public static <T> Result<T> fail(String message, T data) {
        return new Result<>(Code.FAIL, message, data);
    }
}
