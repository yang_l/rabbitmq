package com.example.consumerone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yang_li
 */
@SpringBootApplication
public class ConsumeroneApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumeroneApplication.class, args);
    }

}
