package com.example.consumerone.entity;

import lombok.Data;

/**
 * @Author: yang_li
 * @Date: 2024/3/2917:45
 */

@Data
public class User {

    private String name;

    private Integer age;
}
